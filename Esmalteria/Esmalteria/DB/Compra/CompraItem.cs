﻿using Esmalteria.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Compra
{
   public class CompraItem
    {
        public int Id { get; set; }

        public CompraDTO compra { get; set; }

        public ProdutodecompraDTO Produto { get; set; }


    }
}
