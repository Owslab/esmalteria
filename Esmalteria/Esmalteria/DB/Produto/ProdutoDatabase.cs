﻿using Esmalteria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_Produto (nm_produto, vl_preco, nm_Imagem, id_categoria) 
                                   VALUES (@nm_produto, @vl_preco, @nm_Imagem, @id_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("nm_Imagem", dto.Imagem));
            parms.Add(new MySqlParameter("id_categoria", dto.CategoriaId));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_Produto 
                                 SET nm_produto  = @nm_produto,
                                     vl_preco    = @vl_preco,
                                     nm_Imagem = @nm_Imagem
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("nm_Imagem", dto.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoConsultarView> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_Produto;";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Categoria = reader.GetString("id_Categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                //dto.Imagem = reader.GetString("nm_Imagem");
                dto.CategoriaId = reader.GetInt32("id_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

