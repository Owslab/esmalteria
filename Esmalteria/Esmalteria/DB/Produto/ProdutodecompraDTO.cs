﻿using Esmalteria.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Produto
{
    public class ProdutodecompraDTO
    {
       public int Id { get; set; }
       public string Marca { get; set; }
       public string Nome { get; set; }
       public decimal Preco { get; set; }

        

    }
}
