﻿using Esmalteria.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Cartaodeponto
{
   public class CardaodepontoDTO
    {
        public int Id { get; set; }
        public DateTime entrada { get; set; }
        public TimeSpan ida { get; set; }
        public TimeSpan volta { get; set; }
        public TimeSpan saida { get; set; }

        
    }
}
