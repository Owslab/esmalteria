﻿using Esmalteria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Fornecedor
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (nm_empresa,nu_cnpj,nm_rua,nm_bairro, nu_nlocal,nu_cep,nm_estado,nu_tel,ds_email, tb_funcionario_id_funcionario)
                                              VALUES (@nm_empresa,@nu_cnpj, @nm_rua,@nm_bairro, @nu_nlocal,@nu_cep,@nm_estado,@nu_tel,@ds_email, @tb_funcionario_id_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.FuncionarioId));
            parms.Add(new MySqlParameter("nm_empresa", dto.empresa));
            parms.Add(new MySqlParameter("nu_cnpj", dto.cnpj));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_local", dto.nlocal));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nm_estado", dto.estado));
            parms.Add(new MySqlParameter("nu_tel", dto.tel));
            parms.Add(new MySqlParameter("ds_email", dto.email));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor
                                 SET nm_empresa = @nm_empresa,
                                     nu_cnpj = @nu_cnpj,
                                     nm_rua  = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_nlocal = @nu_nlocal,
                                     nu_cep = @nu_cep,
                                     nm_estado    = @nm_estado,
                                     nu_tel = @nu_tel,
                                     ds_email = @ds_email
                               WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_empresa", dto.empresa));
            parms.Add(new MySqlParameter("nu_cnpj", dto.cnpj));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_local", dto.nlocal));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nm_estado", dto.estado));
            parms.Add(new MySqlParameter("nu_tel", dto.tel));
            parms.Add(new MySqlParameter("ds_email", dto.email));

            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorConsultarView> Consultar(string fornecedor)
        {
            string script = @"SELECT * FROM vw_fornecedor_consultar WHERE nm_empresa like @nm_empresa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorConsultarView> lista = new List<FornecedorConsultarView>();
            while (reader.Read())
            {
                FornecedorConsultarView dto = new FornecedorConsultarView();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.empresa = reader.GetString("nm_empresa");
                dto.cnpj = reader.GetString("nu_cnpj");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.nlocal = reader.GetString("nu_nlocal");
                dto.cep = reader.GetString("nu_cep");
                dto.estado = reader.GetString("nm_estado");
                dto.tel = reader.GetString("nu_tel");
                dto.email = reader.GetString("ds_email");
                dto.Funcionario = reader.GetString("nm_funcionario");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.FuncionarioId));
                parms.Add(new MySqlParameter("nm_empresa", dto.empresa));
                parms.Add(new MySqlParameter("nu_cnpj", dto.cnpj));
                parms.Add(new MySqlParameter("nm_rua", dto.rua));
                parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
                parms.Add(new MySqlParameter("nu_nlocal", dto.nlocal));
                parms.Add(new MySqlParameter("ds_cp", dto.cep));
                parms.Add(new MySqlParameter("nu_cep", dto.estado));
                parms.Add(new MySqlParameter("nu_tel", dto.tel));
                parms.Add(new MySqlParameter("ds_email", dto.email));


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

