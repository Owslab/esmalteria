﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Funcionario
{
    public class FuncionarioDTO
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Funcionario { get; set; }
        public string login { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string CEP { get; set; }
        public string Telefone { get; set; }
        public string RG { get; set; }
        public string Cel { get; set; }
        public string Sexo { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public DateTime DataAdimissao { get; set; }
        public string Cargo { get; set; }
        public string Sálariobruto { get; set; }


        public bool permissao_adm { get; set; }
        public bool permissao_funcionarios { get; set; }
        public bool permissao_cadastrar_funcionarios { get; set; }
        public bool permissao_consultar_funcionarios { get; set; }
        public bool permissao_alterar_funcionario { get; set; }
        public bool permissao_produtos { get; set; }
        public bool permissao_cadastrar_produtos { get; set; }
        public bool permissao_consultar_produtos { get; set; }
        public bool permissao_cadastrar_novo_produto { get; set; }
        public bool permissao_alterar_fornecedor { get; set; }
        public bool permissao_cadastrar_fornecedor { get; set; }
        public bool permissao_consultar_fornecedor { get; set; }
        public bool permissao_pedido_fornecedor { get; set; }
        public bool permissao_consultar_pedido_fornecedor { get; set; }
        public bool permissao_cliente { get; set; }
        public bool permissao_controle_cliente { get; set; }
        public bool permissao_consultar_cliente { get; set; }
        public bool permissao_estoque { get; set; }
        public bool permissao_cadastrar_estoque { get; set; }
        public bool permissao_consultar_estoque { get; set; }
        public bool permissao_cadastrar_categoria { get; set; }
        public bool permissao_consultar_categoria { get; set; }
        public bool permissao_cadastrar_folhadepagamento { get; set; }
        public bool permissao_consultar_folhadepagamento { get; set; }
        public bool permissao_alterar_folhadepagamento { get; set; }
        public bool permissao_pedido { get; set; }
        public bool permissao_consultar_pedido{ get; set; }
        public bool permissao_fluxodecaixa { get; set; }
        public bool permissao_cartaodeponto { get; set; }


    }
}
       
    


