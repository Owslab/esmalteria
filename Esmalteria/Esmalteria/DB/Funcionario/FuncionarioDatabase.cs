﻿using Esmalteria.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Funcionario
{
    public class FuncionarioDatabase
    {
        public FuncionarioDTO Logar(string usuario, string senha)
        {
            string script = $@"SELECT * FROM tb_Funcionario WHERE ds_Usuario = '{usuario}' AND ds_Senha = '{senha}'";

            List<MySqlParameter> parameters = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parameters);
            
            FuncionarioDTO dto = new FuncionarioDTO();
            if (reader.Read() == true)
            {
                //funcioDTO = new FuncionarioDTO();
                //funcioDTO.Id = reader.GetInt32("id_login");
                //funcioDTO.Nome = reader.GetString("nm_usuario");
                //funcioDTO.Usuario = reader.GetString("nm_login");
                //funcioDTO.Senha = reader.GetString("nm_pass");
                //funcioDTO.CPF = reader.GetString("nu_Cpf");
                //funcioDTO.Cargo = reader.GetString("ds_Cargo");
                //funcioDTO.DataAdimissao = reader.GetDateTime("dt_DataAdimissao");
                //funcioDTO.Telefone = reader.GetString("nu_Telefone");
                //funcioDTO.Cel = reader.GetString("nu_Cel");
                //funcioDTO.Cidade = reader.GetString("nm_Cidade");
                //funcioDTO.Endereco = reader.GetString("nm_Endereco");
                //funcioDTO.Sálariobruto = reader.GetString("nm_Sálariobruto");
                //funcioDTO.Sexo = reader.GetString("ds_Sexo");
            

                
                //dto.Id = reader.GetInt32("id_permissao");
                dto.Nome = reader.GetString("nm_Nome");
                dto.permissao_funcionarios = reader.GetBoolean("pr_PermissaoFuncionarios");
                dto.permissao_produtos = reader.GetBoolean("pr_PermissaoProdutos");
                dto.permissao_consultar_pedido_fornecedor= reader.GetBoolean("pr_PermissaoConsultarPedidoFornecedor");
                //dto.permissao_cadastrar_categoria = reader.GetBoolean("bt_vendedor");
                //dto.permissao_consultar_categoria = reader.GetBoolean("bt_vendedor");
                //dto.permissao_estoque = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_estoque = reader.GetBoolean("bt_financeiro");
                //dto.permissao_consultar_estoque = reader.GetBoolean("bt_vendedor");
                //dto.permissao_pedido_fornecedor = reader.GetBoolean("bt_logistica");
                //dto.permissao_cliente = reader.GetBoolean("bt_financeiro");
                //dto.permissao_controle_cliente = reader.GetBoolean("bt_vendedor");
                //dto.permissao_consultar_cliente = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_funcionarios = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_funcionarios = reader.GetBoolean("bt_logistica");
                //dto.permissao_alterar_funcionario = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_novo_produto = reader.GetBoolean("bt_logistica");
                //dto.permissao_produtos = reader.GetBoolean("bt_logistica");
                //dto.permissao_alterar_fornecedor = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_fornecedor  = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_fornecedor = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_produtos = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_produtos = reader.GetBoolean("bt_logistica");
                //dto.permissao_cadastrar_folhadepagamento = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_folhadepagamento = reader.GetBoolean("bt_logistica");
                //dto.permissao_alterar_folhadepagamento = reader.GetBoolean("bt_logistica");
                //dto.permissao_pedido = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_pedido = reader.GetBoolean("bt_logistica");
                //dto.permissao_consultar_pedido_fornecedor = reader.GetBoolean("bt_logistica");
                //dto.permissao_cartaodeponto = reader.GetBoolean("bt_logistica");
                //dto.permissao_fluxodecaixa = reader.GetBoolean("bt_logistica");

            }

               
            return dto;
        }
        public int Salvar (FuncionarioDTO dados)
        {
            string script =
             @"INSERT INTO tb_Funcionario (ds_email, nu_senha, nm_funcionario, nu_rg , nu_cpf , ds_cargo, dt_nascimento, nu_telefone, nu_celular, nm_rua, nm_bairro, nu_numero, nu_cep, ds_sexo, dt_admissao, pr_produto, pr_cliente, pr_estoque, pr_pedido, pr_folha_pagamento, pr_fornecedor, pr_funcionario, pr_fluxo_caixa)
                          VALUES (@ds_email, @nu_senha, @nm_funcionario, @nu_rg , @nu_cpf , @ds_cargo, @dt_nascimento, @nu_telefone, @nu_celular, @nm_rua, @nm_bairro, @nu_numero, @nu_cep, @ds_sexo, @dt_admissao, @pr_produto, @pr_cliente, @pr_estoque, @pr_pedido, @pr_folha_pagamento, @pr_fornecedor, @pr_funcionario, @pr_fluxo_caixa)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_Usuario", dados.Usuario));
            parms.Add(new MySqlParameter("ds_Email", dados.Email));
            parms.Add(new MySqlParameter("nu_Senha", dados.Senha));
            parms.Add(new MySqlParameter("nu_rg", dados.RG));
            parms.Add(new MySqlParameter("nu_cpf", dados.CPF));
            parms.Add(new MySqlParameter("ds_cargo", dados.Cargo));
            parms.Add(new MySqlParameter("dt_Sálariobruto", dados.Sálariobruto));
            parms.Add(new MySqlParameter("nu_telefone", dados.Telefone));
            parms.Add(new MySqlParameter("nu_celular", dados.Cel));
            parms.Add(new MySqlParameter("nm_Cidade", dados.Cidade));
            parms.Add(new MySqlParameter("nm_Endereco", dados.Endereco));
            parms.Add(new MySqlParameter("dt_DataAdimissao", dados.DataAdimissao));
            parms.Add(new MySqlParameter("ds_sexo", dados.Sexo));
            

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FuncionarioDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_Nome");
                dto.RG = reader.GetString("ds_Rg");
                dto.CPF = reader.GetString("ds_Cpf");
                dto.CEP = reader.GetString("ds_Cep");
                dto.Cargo = reader.GetString("ds_Cargo");
                dto.Telefone = reader.GetString("ds_Telefone");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FuncionarioDTO dados)
        {
            string script = @"UPDATE tb_Funcionario 
                                 SET ds_Usuario  = @ds_Usuario,
                                     nu_Senha    = @nu_Senha,
                                     nm_Funcionario = @mn_Funcionario,
                                     nu_RG    = @nu_RG,
                                     nu_CPF = @nu_CPF,
                                     ds_Cargo    = @ds_Cargo,
                                     dt_DataAdmissao = @dt_DataAdmissao,
                                     nu_Telefone    = @nu_Telefone,
                                     nu_Cel = @nu_Cel,
                                     nm_Cidade    = @nm_Cidade,
                                     nm_Sálariobruto = @nm_Sálariobruto,
                                     nm_Endereco = @nm_Endereco,
                                     ds_sexo    = @ds_sexo,
                                     dt_admissao = @dt_admissao,
                                     pr_Produto   = @pr_Produto,
                                     pr_Cliente = @pr_Cliente,
                                     pr_Estoque    = @pr_Estoque,
                                     pr_Pedido = @pr_Pedido,
                                     pr_FdPagamento = @pr_FdPagamento,
                                     pr_Fornecedor = @pr_Fornecedor,
                                     pr_Funcionario = @pr_Funcionario,
                                     pr_Fluxo_caixa = @pr_Fluxo_caixa
                               WHERE id_Funcionario = @id_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_Usuario", dados.Usuario));
            parms.Add(new MySqlParameter("ds_Email", dados.Email));
            parms.Add(new MySqlParameter("nu_Senha", dados.Senha));
            parms.Add(new MySqlParameter("nu_rg", dados.RG));
            parms.Add(new MySqlParameter("nu_cpf", dados.CPF));
            parms.Add(new MySqlParameter("ds_cargo", dados.Cargo));
            parms.Add(new MySqlParameter("dt_Sálariobruto", dados.Sálariobruto));
            parms.Add(new MySqlParameter("nu_telefone", dados.Telefone));
            parms.Add(new MySqlParameter("nu_celular", dados.Cel));
            parms.Add(new MySqlParameter("nm_Cidade", dados.Cidade));
            parms.Add(new MySqlParameter("nm_Endereco", dados.Endereco));
            parms.Add(new MySqlParameter("dt_DataAdimissao", dados.DataAdimissao));
            parms.Add(new MySqlParameter("ds_sexo", dados.Sexo));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Usuario = reader.GetString("ds_Usuario");
                dto.Email = reader.GetString("ds_Email");
                dto.Senha = reader.GetString("nu_Senha");
                dto.Nome = reader.GetString("nm_Nome");
                dto.RG = reader.GetString("nu_RG");
                dto.CPF = reader.GetString("nu_CPF");
                dto.Cargo = reader.GetString("ds_Cargo");
                dto.DataAdimissao = reader.GetDateTime("dt_DataAdmissao");
                dto.Telefone = reader.GetString("nu_Telefone");
                dto.Cel = reader.GetString("nu_Cel");
                dto.Cidade = reader.GetString("nm_Cidade");
                dto.Endereco = reader.GetString("nm_Endereco");
                dto.Sálariobruto = reader.GetString("nm_Sálariobruto");
                dto.Sexo = reader.GetString("ds_sexo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

