﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Funcionario
{
    public class FuncionarioBusiness
    {
        public FuncionarioDTO Logar(string login, string senha)
        {

            if (login == string.Empty)
            {
                throw new ArgumentException("Digite o login");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Digite a senha");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);
        }
        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }

        public List<FuncionarioDTO> Consultar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar();
        }

        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }


        public void Alterar(FuncionarioDTO dados)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dados);
        }

    }
}
