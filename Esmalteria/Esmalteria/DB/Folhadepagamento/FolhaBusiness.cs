﻿using Esmalteria.DB.Folhadepagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esmalteria.DB.Folhadepagamento
{
    class FolhaBusiness
    {

        public int Salvar(FolhaDTO dto)
        {
            FolhaDatabase db = new FolhaDatabase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            FolhaDatabase db = new FolhaDatabase();
            db.Remover(id);
        }

        public void Alterar(FolhaDTO dados)
        {
            FolhaDatabase db = new FolhaDatabase();
            db.Alterar(dados);
        }

        public List<FolhaConsultarView> Consultar(string Nome)
        {
            FolhaDatabase db = new FolhaDatabase();
            return db.Consultar(Nome);
        }

        public List<FolhaDTO> Listar()
        {
            FolhaDatabase db = new FolhaDatabase();
            return db.Listar();
        }
    }
}

