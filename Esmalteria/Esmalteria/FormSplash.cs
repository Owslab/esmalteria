﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria
{
    public partial class FormSplash : Form
    {
        public FormSplash()
        {
            InitializeComponent();

            BackColor = Color.Black;
            TransparencyKey = Color.Black;
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            BackgroundImage = Properties.Resources.Logo;

            Task.Factory.StartNew(() =>
            {
               
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
               
                    FormLogin frm = new FormLogin();
                    frm.Show();
                    Hide();
                }));
            });




        }

        private void FormSplash_Load(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
}
