﻿namespace Esmalteria
{
    partial class FormMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMenu));
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pctFuncionarios = new System.Windows.Forms.PictureBox();
            this.cmsFuncionarios = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarInformaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pctFolha = new System.Windows.Forms.PictureBox();
            this.pctCompras = new System.Windows.Forms.PictureBox();
            this.cmsProdutoCompra = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cadastrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarInformaçõesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pctEstoque = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cmsClientes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarInformaçõesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pctFornecedor = new System.Windows.Forms.PictureBox();
            this.cmsFornecedores = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cadastrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarInformaçõesToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.pctVendas = new System.Windows.Forms.PictureBox();
            this.cmsProdutoVenda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cadastrarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pctFuncionarios)).BeginInit();
            this.cmsFuncionarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctFolha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctCompras)).BeginInit();
            this.cmsProdutoCompra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctEstoque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.cmsClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctFornecedor)).BeginInit();
            this.cmsFornecedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctVendas)).BeginInit();
            this.cmsProdutoVenda.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 480);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(429, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "X";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // pctFuncionarios
            // 
            this.pctFuncionarios.BackColor = System.Drawing.Color.Transparent;
            this.pctFuncionarios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctFuncionarios.BackgroundImage")));
            this.pctFuncionarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctFuncionarios.ContextMenuStrip = this.cmsFuncionarios;
            this.pctFuncionarios.Location = new System.Drawing.Point(18, 94);
            this.pctFuncionarios.Name = "pctFuncionarios";
            this.pctFuncionarios.Size = new System.Drawing.Size(46, 48);
            this.pctFuncionarios.TabIndex = 15;
            this.pctFuncionarios.TabStop = false;
            this.toolTip1.SetToolTip(this.pctFuncionarios, "Rh");
            this.pctFuncionarios.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // cmsFuncionarios
            // 
            this.cmsFuncionarios.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem,
            this.alterarInformaçõesToolStripMenuItem});
            this.cmsFuncionarios.Name = "cmsFuncionarios";
            this.cmsFuncionarios.Size = new System.Drawing.Size(179, 70);
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            // 
            // alterarInformaçõesToolStripMenuItem
            // 
            this.alterarInformaçõesToolStripMenuItem.Name = "alterarInformaçõesToolStripMenuItem";
            this.alterarInformaçõesToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.alterarInformaçõesToolStripMenuItem.Text = "Alterar Informações";
            // 
            // pctFolha
            // 
            this.pctFolha.BackColor = System.Drawing.Color.Transparent;
            this.pctFolha.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctFolha.BackgroundImage")));
            this.pctFolha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctFolha.Location = new System.Drawing.Point(18, 187);
            this.pctFolha.Name = "pctFolha";
            this.pctFolha.Size = new System.Drawing.Size(46, 48);
            this.pctFolha.TabIndex = 16;
            this.pctFolha.TabStop = false;
            this.toolTip1.SetToolTip(this.pctFolha, "Folha de Pagamento");
            this.pctFolha.Click += new System.EventHandler(this.pctFolha_Click);
            // 
            // pctCompras
            // 
            this.pctCompras.BackColor = System.Drawing.Color.Transparent;
            this.pctCompras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctCompras.BackgroundImage")));
            this.pctCompras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctCompras.ContextMenuStrip = this.cmsProdutoCompra;
            this.pctCompras.Location = new System.Drawing.Point(324, 270);
            this.pctCompras.Name = "pctCompras";
            this.pctCompras.Size = new System.Drawing.Size(46, 48);
            this.pctCompras.TabIndex = 23;
            this.pctCompras.TabStop = false;
            this.toolTip1.SetToolTip(this.pctCompras, "Compras ");
            this.pctCompras.Click += new System.EventHandler(this.pctCompras_Click);
            // 
            // cmsProdutoCompra
            // 
            this.cmsProdutoCompra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem2,
            this.consultarToolStripMenuItem2,
            this.alterarInformaçõesToolStripMenuItem2});
            this.cmsProdutoCompra.Name = "cmsProdutoCompra";
            this.cmsProdutoCompra.Size = new System.Drawing.Size(177, 70);
            // 
            // cadastrarToolStripMenuItem2
            // 
            this.cadastrarToolStripMenuItem2.Name = "cadastrarToolStripMenuItem2";
            this.cadastrarToolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.cadastrarToolStripMenuItem2.Text = "Cadastrar Produtos";
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar Produtos";
            // 
            // alterarInformaçõesToolStripMenuItem2
            // 
            this.alterarInformaçõesToolStripMenuItem2.Name = "alterarInformaçõesToolStripMenuItem2";
            this.alterarInformaçõesToolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.alterarInformaçõesToolStripMenuItem2.Text = "Alterar Produtos";
            // 
            // pctEstoque
            // 
            this.pctEstoque.BackColor = System.Drawing.Color.Transparent;
            this.pctEstoque.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctEstoque.BackgroundImage")));
            this.pctEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctEstoque.Location = new System.Drawing.Point(324, 94);
            this.pctEstoque.Name = "pctEstoque";
            this.pctEstoque.Size = new System.Drawing.Size(46, 48);
            this.pctEstoque.TabIndex = 21;
            this.pctEstoque.TabStop = false;
            this.toolTip1.SetToolTip(this.pctEstoque, "Estoque");
            this.pctEstoque.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Location = new System.Drawing.Point(-7, 1);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(39, 25);
            this.pictureBox7.TabIndex = 30;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
            // 
            // cmsClientes
            // 
            this.cmsClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.consultarToolStripMenuItem1,
            this.alterarInformaçõesToolStripMenuItem1});
            this.cmsClientes.Name = "cmsClientes";
            this.cmsClientes.Size = new System.Drawing.Size(179, 70);
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            // 
            // alterarInformaçõesToolStripMenuItem1
            // 
            this.alterarInformaçõesToolStripMenuItem1.Name = "alterarInformaçõesToolStripMenuItem1";
            this.alterarInformaçõesToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.alterarInformaçõesToolStripMenuItem1.Text = "Alterar Informações";
            // 
            // pctFornecedor
            // 
            this.pctFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.pctFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctFornecedor.BackgroundImage")));
            this.pctFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctFornecedor.ContextMenuStrip = this.cmsFornecedores;
            this.pctFornecedor.Location = new System.Drawing.Point(18, 270);
            this.pctFornecedor.Name = "pctFornecedor";
            this.pctFornecedor.Size = new System.Drawing.Size(46, 48);
            this.pctFornecedor.TabIndex = 34;
            this.pctFornecedor.TabStop = false;
            this.toolTip1.SetToolTip(this.pctFornecedor, "Fornecedor");
            this.pctFornecedor.Click += new System.EventHandler(this.pctFornecedor_Click);
            // 
            // cmsFornecedores
            // 
            this.cmsFornecedores.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem3,
            this.consultarToolStripMenuItem3,
            this.alterarInformaçõesToolStripMenuItem3});
            this.cmsFornecedores.Name = "cmsFornecedores";
            this.cmsFornecedores.Size = new System.Drawing.Size(179, 70);
            // 
            // cadastrarToolStripMenuItem3
            // 
            this.cadastrarToolStripMenuItem3.Name = "cadastrarToolStripMenuItem3";
            this.cadastrarToolStripMenuItem3.Size = new System.Drawing.Size(178, 22);
            this.cadastrarToolStripMenuItem3.Text = "Cadastrar";
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(178, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            // 
            // alterarInformaçõesToolStripMenuItem3
            // 
            this.alterarInformaçõesToolStripMenuItem3.Name = "alterarInformaçõesToolStripMenuItem3";
            this.alterarInformaçõesToolStripMenuItem3.Size = new System.Drawing.Size(178, 22);
            this.alterarInformaçõesToolStripMenuItem3.Text = "Alterar Informações";
            // 
            // pctVendas
            // 
            this.pctVendas.BackColor = System.Drawing.Color.Transparent;
            this.pctVendas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctVendas.BackgroundImage")));
            this.pctVendas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctVendas.ContextMenuStrip = this.cmsProdutoVenda;
            this.pctVendas.Location = new System.Drawing.Point(324, 169);
            this.pctVendas.Name = "pctVendas";
            this.pctVendas.Size = new System.Drawing.Size(46, 48);
            this.pctVendas.TabIndex = 37;
            this.pctVendas.TabStop = false;
            this.toolTip1.SetToolTip(this.pctVendas, "Vendas");
            this.pctVendas.Click += new System.EventHandler(this.pctVendas_Click);
            // 
            // cmsProdutoVenda
            // 
            this.cmsProdutoVenda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarProdutoToolStripMenuItem,
            this.consultarProdutoToolStripMenuItem,
            this.alterarProdutoToolStripMenuItem});
            this.cmsProdutoVenda.Name = "cmsProdutoVenda";
            this.cmsProdutoVenda.Size = new System.Drawing.Size(172, 70);
            // 
            // cadastrarProdutoToolStripMenuItem
            // 
            this.cadastrarProdutoToolStripMenuItem.Name = "cadastrarProdutoToolStripMenuItem";
            this.cadastrarProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.cadastrarProdutoToolStripMenuItem.Text = "Cadastrar Produto";
            // 
            // consultarProdutoToolStripMenuItem
            // 
            this.consultarProdutoToolStripMenuItem.Name = "consultarProdutoToolStripMenuItem";
            this.consultarProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.consultarProdutoToolStripMenuItem.Text = "Consultar Produto";
            // 
            // alterarProdutoToolStripMenuItem
            // 
            this.alterarProdutoToolStripMenuItem.Name = "alterarProdutoToolStripMenuItem";
            this.alterarProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.alterarProdutoToolStripMenuItem.Text = "Alterar Produto";
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(419, 402);
            this.Controls.Add(this.pctVendas);
            this.Controls.Add(this.pctFornecedor);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pctCompras);
            this.Controls.Add(this.pctEstoque);
            this.Controls.Add(this.pctFolha);
            this.Controls.Add(this.pctFuncionarios);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctFuncionarios)).EndInit();
            this.cmsFuncionarios.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctFolha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctCompras)).EndInit();
            this.cmsProdutoCompra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctEstoque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.cmsClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctFornecedor)).EndInit();
            this.cmsFornecedores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctVendas)).EndInit();
            this.cmsProdutoVenda.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pctFuncionarios;
        private System.Windows.Forms.PictureBox pctFolha;
        private System.Windows.Forms.PictureBox pctCompras;
        private System.Windows.Forms.PictureBox pctEstoque;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip cmsFuncionarios;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarInformaçõesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsProdutoCompra;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem alterarInformaçõesToolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip cmsClientes;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alterarInformaçõesToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pctFornecedor;
        private System.Windows.Forms.ContextMenuStrip cmsFornecedores;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem alterarInformaçõesToolStripMenuItem3;
        private System.Windows.Forms.PictureBox pctVendas;
        private System.Windows.Forms.ContextMenuStrip cmsProdutoVenda;
        private System.Windows.Forms.ToolStripMenuItem cadastrarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarProdutoToolStripMenuItem;
    }
}