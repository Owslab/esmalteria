﻿using Esmalteria.DB.Folhadepagamento;
using Esmalteria.Telas.Folha_de_pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas.Folha_de_pagamento
{
    public partial class FormConsultarFdPagamento : Form
    {
        public FormConsultarFdPagamento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolhaBusiness business = new FolhaBusiness();
            List<FolhaConsultarView> lista = business.Consultar(txtFolhaPagamento.Text);

            dgvFolhadePagamento.AutoGenerateColumns = false;
            dgvFolhadePagamento.DataSource = lista;
        }

        private void dgvFolhadePagamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 9)
                {
                    FolhaConsultarView cat = dgvFolhadePagamento.CurrentRow.DataBoundItem as FolhaConsultarView;

                    FolhaBusiness business = new FolhaBusiness();
                    business.Remover(cat.Id);

                    button1_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse não pode ser removido agora");
                FormConsultarFdPagamento ir = new FormConsultarFdPagamento();
                this.Hide();
                ir.ShowDialog();
            }
        }

        private void dgvFolhadePagamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtFolhaPagamento_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }
    }
}