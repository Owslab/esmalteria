﻿using Esmalteria.DB.Folhadepagamento;
using Esmalteria.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas.Folha_de_pagamento
{
    public partial class FormFdPagamento : Form
    {
        public FormFdPagamento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                FolhaDTO dto = new FolhaDTO();
                dto.FuncionarioId = Fun.Id;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.horames = txtHoraMes.Text;
                dto.salariohora = txtSalarioHora.Text;
                dto.valor = txtValor.Text;
                dto.aliquota = txtAliquota.Text;
                dto.inss = txtInss.Text;
                dto.salariofinal = txtSalarioFinal.Text;
                dto.horaext50 = txtHr1.Text;
                dto.horaext100 = txtHr2.Text;
                dto.vlrefeicao = txtRefeicao.Text;
                dto.vltransporte = txtTransporte.Text;

                if (txtHoraMes.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a quantidades de horas trabalhadas!");
                    return;
                }

                if (txtSalarioHora.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar o valor do salário hora!");
                    return;
                }

                if (txtAliquota.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a aliquota para calculo do inss!");
                    return;
                }

                txtValor.Text = Convert.ToString((Convert.ToDecimal(txtHoraMes.Text)) * (Convert.ToDecimal(txtSalarioHora.Text)));
                dto.valor = txtValor.Text;

                txtInss.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) * ((Convert.ToDecimal(txtAliquota.Text)) / 100));
                dto.inss = txtInss.Text;

                txtSalarioFinal.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) - (Convert.ToDecimal(txtInss.Text)));
                dto.salariofinal = txtSalarioFinal.Text;


                FolhaBusiness business = new FolhaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Folha de pagamento salvo com sucesso.", "Esmalteria Glamour", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas.", "Esmalteria Glamour", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FormTeste_Load(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormAlterarFdPagamento menu = new FormAlterarFdPagamento();
            menu.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormConsultarFdPagamento menu = new FormConsultarFdPagamento();
            menu.Show();
            this.Hide();
        }
        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void frmFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            FuncionarioDTO dto = cboFuncionario.SelectedItem as FuncionarioDTO;
        }

        private void txtValor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHoraMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtSalarioHora_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtAliquota_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr2_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtRefeicao_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtTransporte_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }
    }
 }

