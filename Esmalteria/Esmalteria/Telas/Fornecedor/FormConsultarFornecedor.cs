﻿using Esmalteria.DB.Fornecedor;
using Esmalteria.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas.Fornecedor
{
    public partial class FormConsultarFornecedor : Form
    {
        public FormConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorConsultarView> lista = business.Consultar(textBox3.Text);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = lista;
        }

        private void dgvFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 13)
                {
                    FornecedorConsultarView cat = dgvFornecedor.CurrentRow.DataBoundItem as FornecedorConsultarView;

                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(cat.Id);

                    button1_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse fornecedor não pode ser removido agora");
                FormConsultarFornecedor ir = new FormConsultarFornecedor();
                return;
            }
        }
    }
}
