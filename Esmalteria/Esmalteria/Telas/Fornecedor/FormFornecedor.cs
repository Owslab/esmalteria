﻿using Esmalteria;
using Esmalteria.DB.Fornecedor;
using Esmalteria.DB.Funcionario;
using Esmalteria.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria
{
    public partial class FormFornecedor : Form
    {
        public FormFornecedor()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            FormAlterarFornecedor ir = new FormAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            FormConsultarFornecedor ir = new FormConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                FornecedorDTO dto = new FornecedorDTO();
                dto.FuncionarioId = Fun.Id;
                dto.empresa = txtnome.Text.Trim();
                dto.cnpj = txtCNPJ.Text;
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.nlocal = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.estado = cboEstado.Text.Trim();
                dto.tel = txtTelefone.Text.Trim();
                dto.email = txtEmail.Text.Trim();

                if (dto.empresa == string.Empty || dto.empresa == "Empresa")
                {
                    MessageBox.Show("Digite o nome da empresa");

                    return;
                }
                if (dto.cnpj == string.Empty || txtCNPJ.Text.Length <= 17)
                {
                    MessageBox.Show("Digite seu CNPJ");

                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro")
                {
                    MessageBox.Show("Digite seu bairro");

                    return;
                }


                if (dto.nlocal == string.Empty || dto.nlocal == "Digite o numero da sua casa")
                {
                    MessageBox.Show("Digite o numero da sua casa");

                    return;
                }

                if (dto.cep == string.Empty || dto.cep == "Digite seu cep" || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite o cep");

                    return;
                }

                if (dto.estado == string.Empty || dto.estado == "Digite seu estado")
                {
                    MessageBox.Show("Digite sua estado");

                    return;
                }


                if (dto.tel == string.Empty || dto.tel == "Digite seu telefone" || txtTelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Digite o telefone");

                    return;
                }


                if (dto.email == string.Empty || dto.email == "Digite seu Email")
                {
                    MessageBox.Show("Digite o seu email");

                    return;
                }

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Fornecedor salvo com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas tente novamente");
                return;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormMenu ir = new FormMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu ir = new FormMenu();
            this.Hide();
            ir.ShowDialog();
        }
    }
    }
  

