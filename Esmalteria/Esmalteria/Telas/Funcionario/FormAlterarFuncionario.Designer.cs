﻿namespace Esmalteria.Telas
{
    partial class FormAlterarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboSexo = new System.Windows.Forms.ComboBox();
            this.maskedTextBox5 = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDateAdmissao = new System.Windows.Forms.MaskedTextBox();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboCidade = new System.Windows.Forms.ComboBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.txtCel = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.rdnPermissaoFornecedor = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoProdutos = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoFluxodeCaixa = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoAdminidtrativo = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoFolhadePagamento = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoEstoque = new System.Windows.Forms.RadioButton();
            this.rdnPermissaoFuncionario = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkRed;
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(668, 39);
            this.panel2.TabIndex = 226;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(643, 9);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 20);
            this.label17.TabIndex = 15;
            this.label17.Text = "X";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(4, 9);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(143, 20);
            this.label18.TabIndex = 2;
            this.label18.Text = "Alterar Funcionario";
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Location = new System.Drawing.Point(528, 579);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 28);
            this.btnCancelar.TabIndex = 277;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Location = new System.Drawing.Point(410, 579);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(100, 28);
            this.btnSalvar.TabIndex = 276;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(191, 376);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(207, 20);
            this.textBox3.TabIndex = 361;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(116, 377);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 360;
            this.label1.Text = "Usuário:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(126, 414);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 16);
            this.label19.TabIndex = 359;
            this.label19.Text = "Senha:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(127, 344);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 16);
            this.label11.TabIndex = 358;
            this.label11.Text = "E-mail:";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Location = new System.Drawing.Point(192, 409);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(206, 24);
            this.textBox2.TabIndex = 357;
            this.textBox2.Text = "123456789";
            this.textBox2.UseSystemPasswordChar = true;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.White;
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(191, 341);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(399, 22);
            this.txtEmail.TabIndex = 356;
            this.txtEmail.Text = "Crie um E-mail";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(378, 144);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 16);
            this.label10.TabIndex = 355;
            this.label10.Text = "Sexo:";
            // 
            // cboSexo
            // 
            this.cboSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSexo.FormattingEnabled = true;
            this.cboSexo.Items.AddRange(new object[] {
            "Maculino",
            "Feminino"});
            this.cboSexo.Location = new System.Drawing.Point(426, 142);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Size = new System.Drawing.Size(107, 21);
            this.cboSexo.TabIndex = 354;
            // 
            // maskedTextBox5
            // 
            this.maskedTextBox5.Location = new System.Drawing.Point(191, 303);
            this.maskedTextBox5.Margin = new System.Windows.Forms.Padding(4);
            this.maskedTextBox5.Mask = "00000";
            this.maskedTextBox5.Name = "maskedTextBox5";
            this.maskedTextBox5.Size = new System.Drawing.Size(171, 20);
            this.maskedTextBox5.TabIndex = 353;
            this.maskedTextBox5.ValidatingType = typeof(int);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(95, 307);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 16);
            this.label8.TabIndex = 352;
            this.label8.Text = "Sálario bruto:";
            // 
            // txtDateAdmissao
            // 
            this.txtDateAdmissao.Location = new System.Drawing.Point(490, 267);
            this.txtDateAdmissao.Mask = "00/00/0000";
            this.txtDateAdmissao.Name = "txtDateAdmissao";
            this.txtDateAdmissao.Size = new System.Drawing.Size(100, 20);
            this.txtDateAdmissao.TabIndex = 351;
            this.txtDateAdmissao.ValidatingType = typeof(System.DateTime);
            // 
            // cboEstado
            // 
            this.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Items.AddRange(new object[] {
            "Acre (AC) - Rio Branco",
            "Alagoas (AL) - Maceió",
            "Amapá (AP) - Macapá",
            "Amazonas (AM) - Manaus",
            "Bahia (BA) - Salvador",
            "Ceará (CE) - Fortaleza",
            "Distrito Federal (DF) - Brasília",
            "Espírito Santo (ES) - Vitória",
            "Goiás (GO) - Goiânia",
            "Maranhão (MA) - São Luís",
            "Mato Grosso (MT) - Cuiabá",
            "Mato Grosso do Sul (MS) - Campo Grande",
            "Minas Gerais (MG) - Belo Horizonte",
            "Pará (PA) - Belém",
            "Paraíba (PB) - João Pessoa",
            "Paraná (PR) - Curitiba",
            "Pernambuco (PE) - Recife",
            "Piauí (PI) - Teresina",
            "Rio de Janeiro (RJ) - Rio de Janeiro",
            "Rio Grande do Norte (RN) - Natal",
            "Rio Grande do Sul (RS) - Porto Alegre",
            "Rondônia (RO) - Porto Velho",
            "Roraima (RR) - Boa Vista",
            "Santa Catarina (SC) - Florianópolis",
            "São Paulo (SP) - São Paulo",
            "Sergipe (SE) - Aracaju",
            "Tocantins (TO) - Palmas"});
            this.cboEstado.Location = new System.Drawing.Point(420, 218);
            this.cboEstado.Margin = new System.Windows.Forms.Padding(4);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(171, 21);
            this.cboEstado.TabIndex = 350;
            // 
            // cboCargo
            // 
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Items.AddRange(new object[] {
            "Recepcionista",
            "Recepcionista",
            "Gerente de Vendas",
            "Auxiliar Administrativo",
            "Caixa",
            "Caixa",
            "Gerente Administrativa",
            "Segurança",
            "Manicure",
            "Manicure",
            "Manicure",
            "Manicure",
            "Limpeza",
            "Limpeza",
            "Podologa "});
            this.cboCargo.Location = new System.Drawing.Point(191, 266);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(171, 21);
            this.cboCargo.TabIndex = 349;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(126, 273);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 348;
            this.label7.Text = "Cargo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(370, 273);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 347;
            this.label5.Text = "Data Admissão:";
            // 
            // cboCidade
            // 
            this.cboCidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCidade.FormattingEnabled = true;
            this.cboCidade.Items.AddRange(new object[] {
            "São Paulo\t",
            "Guarulhos\t",
            "Campinas",
            "São Bernardo do Campo\t",
            "Santo André\t",
            "São José dos Campos\t",
            "Osasco",
            "Ribeirão Preto\t",
            "Sorocaba",
            "Mauá\t\t",
            "São José do Rio Preto\t",
            "Santos\t",
            "Mogi das Cruzes\t",
            "Diadema\t",
            "Jundiaí\t\t",
            "Piracicaba\t",
            "Carapicuíba\t",
            "Bauru\t",
            "Itaquaquecetuba\t",
            "São Vicente\t",
            "Franca\t",
            "Guarujá\t",
            "Praia Grande\t",
            "Taubaté\t",
            "Limeira\t",
            "Suzano\t",
            "Taboão da Serra\t",
            "Sumaré",
            "Barueri\t",
            "Embu das Artes\t\t",
            "São Carlos\t",
            "Indaiatuba\t",
            "Cotia\t",
            "Marília\t",
            "Americana\t",
            "Araraquara\t",
            "Jacareí\t",
            "Itapevi\t",
            "Presidente Prudente\t",
            "Hortolândia\t",
            "Rio Claro\t",
            ""});
            this.cboCidade.Location = new System.Drawing.Point(190, 221);
            this.cboCidade.Margin = new System.Windows.Forms.Padding(4);
            this.cboCidade.Name = "cboCidade";
            this.cboCidade.Size = new System.Drawing.Size(171, 21);
            this.cboCidade.TabIndex = 346;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(190, 182);
            this.txtEndereco.Margin = new System.Windows.Forms.Padding(4);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(401, 20);
            this.txtEndereco.TabIndex = 345;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(363, 226);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 344;
            this.label4.Text = "Estado:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(121, 226);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 16);
            this.label3.TabIndex = 343;
            this.label3.Text = "Cidade:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(112, 186);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 342;
            this.label2.Text = "Endereço:";
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(191, 147);
            this.txtCEP.Margin = new System.Windows.Forms.Padding(4);
            this.txtCEP.Mask = "   .   .   -";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(177, 20);
            this.txtCEP.TabIndex = 341;
            // 
            // txtCel
            // 
            this.txtCel.Location = new System.Drawing.Point(414, 116);
            this.txtCel.Margin = new System.Windows.Forms.Padding(4);
            this.txtCel.Mask = "(999) 000-0000";
            this.txtCel.Name = "txtCel";
            this.txtCel.Size = new System.Drawing.Size(177, 20);
            this.txtCel.TabIndex = 340;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(414, 78);
            this.txtRG.Margin = new System.Windows.Forms.Padding(4);
            this.txtRG.Mask = "  .   .   -";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(177, 20);
            this.txtRG.TabIndex = 339;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(191, 113);
            this.txtTelefone.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefone.Mask = "(999) 000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(177, 20);
            this.txtTelefone.TabIndex = 338;
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(191, 80);
            this.txtCPF.Margin = new System.Windows.Forms.Padding(4);
            this.txtCPF.Mask = "   .   .   -";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(177, 20);
            this.txtCPF.TabIndex = 337;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(371, 116);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 16);
            this.label14.TabIndex = 336;
            this.label14.Text = "Cel:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(371, 84);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 16);
            this.label13.TabIndex = 335;
            this.label13.Text = "RG:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(148, 84);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 16);
            this.label12.TabIndex = 334;
            this.label12.Text = "CPF:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(149, 147);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 16);
            this.label9.TabIndex = 333;
            this.label9.Text = "Cep:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(119, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 332;
            this.label6.Text = "Telefone:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(191, 48);
            this.txtNome.Margin = new System.Windows.Forms.Padding(4);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(400, 20);
            this.txtNome.TabIndex = 331;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(74, 54);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(109, 16);
            this.label21.TabIndex = 330;
            this.label21.Text = "Nome Completo:";
            // 
            // rdnPermissaoFornecedor
            // 
            this.rdnPermissaoFornecedor.AutoSize = true;
            this.rdnPermissaoFornecedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoFornecedor.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoFornecedor.Location = new System.Drawing.Point(6, 471);
            this.rdnPermissaoFornecedor.Name = "rdnPermissaoFornecedor";
            this.rdnPermissaoFornecedor.Size = new System.Drawing.Size(184, 20);
            this.rdnPermissaoFornecedor.TabIndex = 387;
            this.rdnPermissaoFornecedor.TabStop = true;
            this.rdnPermissaoFornecedor.Text = "Permissão Fornecedor";
            this.rdnPermissaoFornecedor.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoProdutos
            // 
            this.rdnPermissaoProdutos.AutoSize = true;
            this.rdnPermissaoProdutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoProdutos.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoProdutos.Location = new System.Drawing.Point(6, 497);
            this.rdnPermissaoProdutos.Name = "rdnPermissaoProdutos";
            this.rdnPermissaoProdutos.Size = new System.Drawing.Size(166, 20);
            this.rdnPermissaoProdutos.TabIndex = 386;
            this.rdnPermissaoProdutos.TabStop = true;
            this.rdnPermissaoProdutos.Text = "Permissão Produtos";
            this.rdnPermissaoProdutos.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoFluxodeCaixa
            // 
            this.rdnPermissaoFluxodeCaixa.AutoSize = true;
            this.rdnPermissaoFluxodeCaixa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoFluxodeCaixa.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoFluxodeCaixa.Location = new System.Drawing.Point(401, 494);
            this.rdnPermissaoFluxodeCaixa.Name = "rdnPermissaoFluxodeCaixa";
            this.rdnPermissaoFluxodeCaixa.Size = new System.Drawing.Size(204, 20);
            this.rdnPermissaoFluxodeCaixa.TabIndex = 385;
            this.rdnPermissaoFluxodeCaixa.TabStop = true;
            this.rdnPermissaoFluxodeCaixa.Text = "Permissão Fluxo de caixa";
            this.rdnPermissaoFluxodeCaixa.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoAdminidtrativo
            // 
            this.rdnPermissaoAdminidtrativo.AutoSize = true;
            this.rdnPermissaoAdminidtrativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoAdminidtrativo.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoAdminidtrativo.Location = new System.Drawing.Point(194, 471);
            this.rdnPermissaoAdminidtrativo.Name = "rdnPermissaoAdminidtrativo";
            this.rdnPermissaoAdminidtrativo.Size = new System.Drawing.Size(202, 20);
            this.rdnPermissaoAdminidtrativo.TabIndex = 384;
            this.rdnPermissaoAdminidtrativo.TabStop = true;
            this.rdnPermissaoAdminidtrativo.Text = "Permissão Administrativo";
            this.rdnPermissaoAdminidtrativo.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoFolhadePagamento
            // 
            this.rdnPermissaoFolhadePagamento.AutoSize = true;
            this.rdnPermissaoFolhadePagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoFolhadePagamento.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoFolhadePagamento.Location = new System.Drawing.Point(402, 471);
            this.rdnPermissaoFolhadePagamento.Name = "rdnPermissaoFolhadePagamento";
            this.rdnPermissaoFolhadePagamento.Size = new System.Drawing.Size(248, 20);
            this.rdnPermissaoFolhadePagamento.TabIndex = 383;
            this.rdnPermissaoFolhadePagamento.TabStop = true;
            this.rdnPermissaoFolhadePagamento.Text = "Permissão Folha de Pagamento";
            this.rdnPermissaoFolhadePagamento.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoEstoque
            // 
            this.rdnPermissaoEstoque.AutoSize = true;
            this.rdnPermissaoEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoEstoque.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoEstoque.Location = new System.Drawing.Point(6, 523);
            this.rdnPermissaoEstoque.Name = "rdnPermissaoEstoque";
            this.rdnPermissaoEstoque.Size = new System.Drawing.Size(170, 20);
            this.rdnPermissaoEstoque.TabIndex = 382;
            this.rdnPermissaoEstoque.TabStop = true;
            this.rdnPermissaoEstoque.Text = "Permissaão Estoque";
            this.rdnPermissaoEstoque.UseVisualStyleBackColor = true;
            // 
            // rdnPermissaoFuncionario
            // 
            this.rdnPermissaoFuncionario.AutoSize = true;
            this.rdnPermissaoFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPermissaoFuncionario.ForeColor = System.Drawing.Color.Black;
            this.rdnPermissaoFuncionario.Location = new System.Drawing.Point(194, 494);
            this.rdnPermissaoFuncionario.Name = "rdnPermissaoFuncionario";
            this.rdnPermissaoFuncionario.Size = new System.Drawing.Size(185, 20);
            this.rdnPermissaoFuncionario.TabIndex = 381;
            this.rdnPermissaoFuncionario.TabStop = true;
            this.rdnPermissaoFuncionario.Text = "Permissão Funcionario";
            this.rdnPermissaoFuncionario.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(287, 447);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 20);
            this.label20.TabIndex = 380;
            this.label20.Text = "Permissão:";
            // 
            // FormAlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 620);
            this.Controls.Add(this.rdnPermissaoFornecedor);
            this.Controls.Add(this.rdnPermissaoProdutos);
            this.Controls.Add(this.rdnPermissaoFluxodeCaixa);
            this.Controls.Add(this.rdnPermissaoAdminidtrativo);
            this.Controls.Add(this.rdnPermissaoFolhadePagamento);
            this.Controls.Add(this.rdnPermissaoEstoque);
            this.Controls.Add(this.rdnPermissaoFuncionario);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cboSexo);
            this.Controls.Add(this.maskedTextBox5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDateAdmissao);
            this.Controls.Add(this.cboEstado);
            this.Controls.Add(this.cboCargo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboCidade);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.txtCel);
            this.Controls.Add(this.txtRG);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAlterarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormAlterarFuncionario_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboSexo;
        private System.Windows.Forms.MaskedTextBox maskedTextBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtDateAdmissao;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboCidade;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.MaskedTextBox txtCel;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton rdnPermissaoFornecedor;
        private System.Windows.Forms.RadioButton rdnPermissaoProdutos;
        private System.Windows.Forms.RadioButton rdnPermissaoFluxodeCaixa;
        private System.Windows.Forms.RadioButton rdnPermissaoAdminidtrativo;
        private System.Windows.Forms.RadioButton rdnPermissaoFolhadePagamento;
        private System.Windows.Forms.RadioButton rdnPermissaoEstoque;
        private System.Windows.Forms.RadioButton rdnPermissaoFuncionario;
        private System.Windows.Forms.Label label20;
    }
}