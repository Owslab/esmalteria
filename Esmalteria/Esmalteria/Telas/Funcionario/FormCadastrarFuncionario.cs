﻿using Esmalteria.DB.Funcionario;
using Esmalteria.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria
{
    public partial class FormCadastrarFuncionario : Form
    {
        public FormCadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void FormCadastrarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
       
            FormAlterarFuncionario trocar = new FormAlterarFuncionario();
            trocar.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO user = UserSession.UsuarioLogado;
                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");

                if (user.permissao_cadastrar_funcionarios == false)
                    throw new ArgumentException("Você não tem permissão para isso!");







                FuncionarioDTO dto = new FuncionarioDTO();

                dto.Usuario = txtEmail.Text.Trim();
                dto.Senha = txtSenha.Text.Trim();
                dto.Nome = txtNome.Text.Trim();
                dto.RG = txtRG.Text.Trim();
                dto.CPF = txtCPF.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.Cargo = cboCargo.Text;
                dto.DataAdimissao = Convert.ToDateTime(txtDateAdmissao.Text);
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Cel = txtCel.Text.Trim();
                dto.Endereco = txtEndereco.Text.Trim();
                dto.Cidade = cboCidade.Text.Trim();
                dto.Estado = cboEstado.Text.Trim();
                dto.Sálariobruto = txtCPF.Text.Trim();
                dto.Sexo = cboSexo.Text.Trim();

                if (dto.Email == string.Empty || dto.Email == "Crie um E-mail")
                {
                    MessageBox.Show("Crie um e-mail para login.");
                    return;
                }

                if (dto.Senha == string.Empty)
                {
                    MessageBox.Show("Crie uma senha para login.");
                    return;
                }

                if (dto.Funcionario == string.Empty || dto.Funcionario == "Nome Completo")
                {
                    MessageBox.Show("Coloque o nome do funcionario.");
                    return;
                }

                if (dto.RG == string.Empty || txtRG.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros da RG.");
                    return;
                }

                if (dto.CPF == string.Empty || txtCPF.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros do CPF.");
                    return;
                }

                if (dto.CEP == string.Empty || txtCEP.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }

                if (dto.Cargo == string.Empty)
                {
                    MessageBox.Show("Selecione o cargo.");
                    return;
                }

                if (dto.Telefone == string.Empty || txtTelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }

                if (dto.Cel == string.Empty || txtCel.Text.Length <= 14)
                {
                    MessageBox.Show("Celular é obrigatório.");
                    return;
                }

                if (dto.Endereco == string.Empty || dto.Endereco == "Endereço que mora")
                {
                    MessageBox.Show("Endereço é obrigatória.");
                    return;
                }

                if (dto.Sálariobruto == string.Empty)
                {
                    MessageBox.Show(" Esse campo só Administrador pode mexer.");
                    return;
                }


                if (dto.Cidade == string.Empty)
                {
                    MessageBox.Show("Selecione sua Cidade.");
                    return;
                }

                if (dto.Sexo == string.Empty)
                {
                    MessageBox.Show("Selecione o Sexo.");
                    return;
                }
                if (dto.Estado == string.Empty)
                {
                    MessageBox.Show("Selecione o seu Estado.");
                    return;
                }

                FuncionarioBusiness mds = new FuncionarioBusiness();
                mds.Salvar(dto);

                MessageBox.Show("Funcionario salvo.", "Esmalteria Glamour",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                FormLogin ir = new FormLogin();
                this.Hide();
                ir.ShowDialog();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            FormProcurarFuncionario ir = new FormProcurarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            txtNome.Text = string.Empty;
        }

        private void rdnauxlimpeza_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdnrh_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdnfinancias_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_MouseClick(object sender, MouseEventArgs e)
        {


            textBox2.Text = string.Empty;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged_1(object sender, EventArgs e)
        {
            
        }

        private void txtEmail_MouseClick(object sender, MouseEventArgs e)
        {
            txtEmail.Text = string.Empty; 
        }
    }
  }

