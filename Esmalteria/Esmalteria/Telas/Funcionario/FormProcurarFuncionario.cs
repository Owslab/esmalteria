﻿using Esmalteria.DB.Funcionario;
using Esmalteria.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas
{
    public partial class FormProcurarFuncionario : Form
    {
        public FormProcurarFuncionario()
        {
            InitializeComponent();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Consultar();

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void dgvFuncionario_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 15)
                {
                    FuncionarioDTO cat = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(cat.Id);

                    button1_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos funcionarios que não pode ser removido");
                FormProcurarFuncionario ir = new FormProcurarFuncionario();
                this.Hide();
                ir.ShowDialog();
               }
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
