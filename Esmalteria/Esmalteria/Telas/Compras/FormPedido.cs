﻿using Esmalteria.DB.Cliente;
using Esmalteria.DB.Funcionario;
using Esmalteria.DB.Pedido;
using Esmalteria.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas.Pedido
{
    public partial class FormPedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        BindingList<ClienteDTO> clientes = new BindingList<ClienteDTO>();


        public FormPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

                int qtd = Convert.ToInt32(txtQuantidade.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception )
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = cboCliente.SelectedItem as ClienteDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                dto.ClienteId = cliente.Id;
                dto.FormaPagamento = cboFormaPag.Text;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "Esmalteria Glamour", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception )
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmClienteCadastrar tela = new frmClienteCadastrar();
            tela.ShowDialog();

          

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
                imgProduto.Image = Imagem.ConverterParaImagem(dto.Imagem);
            }
            catch
            {
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void FormPedido_Load(object sender, EventArgs e)
        {

        }

        private void imgProduto_Click(object sender, EventArgs e)
        {

        }

       
            void CarregarCombos()
        {
                //cboFormaPag.SelectedIndex = 0;


                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoDTO> lista = business.Listar();

                cboProduto.ValueMember = nameof(ProdutoDTO.Id);
                cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
                cboProduto.DataSource = lista;



                ClienteBusiness clienteBusiness = new ClienteBusiness();
                List<ClienteDTO> clienteLista = clienteBusiness.Listar();
                clientes = new BindingList<ClienteDTO>(clienteLista);

                cboCliente.ValueMember = nameof(ClienteDTO.Id);
                cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
                cboCliente.DataSource = clientes;
            }
        }
    }
  

