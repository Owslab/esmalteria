﻿using Esmalteria.DB.Categoria;
using Esmalteria.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria.Telas.Produto
{
    public partial class FormProdutoCadastrar : Form
    {
        public FormProdutoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
        }



        void CarregarCombos()
        {
            CategoriaBusiness bus = new CategoriaBusiness();
            List<CategoriaDTO> lista = bus.Listar();

            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.DataSource = lista;
        }



        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO categoria = cboCategoria.SelectedItem as CategoriaDTO;

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = Convert.ToDecimal(txtPreco.Text.Trim());
                dto.Imagem = Imagem.ConverterParaString(imgProduto.Image);
                dto.CategoriaId = categoria.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.", "Esmalteria Glamour",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception )
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Esmalteria Glamour",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void FormProdutoCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgProduto.ImageLocation = dialog.FileName;
            }
        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCategoria_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormProdutoConsultar frm = new FormProdutoConsultar();
            frm.Show();
            this.Hide();
        }
    }
}

