﻿using Esmalteria.DB.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria
{
    public partial class frmCategoriaCadastrar : Form
    {
        public frmCategoriaCadastrar()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Nome = txtCategoria.Text.Trim();

                CategoriaBusiness business = new CategoriaBusiness();
                business.Salvar(dto);

                MessageBox.Show("Categoria salva com sucesso.", "Esmalteria",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Esmalteria",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            FormMenu voltar = new FormMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void frmCategoriaCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void txtCategoria_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
