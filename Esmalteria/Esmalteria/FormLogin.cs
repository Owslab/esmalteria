﻿using Esmalteria;
using Esmalteria.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Esmalteria
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            FuncionarioDTO dto = db.Logar(txtNome.Text, txtSenha.Text);

            if (dto.Nome == string.Empty)
            {
                MessageBox.Show("Ocorreu um erro.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                Hide();
                MessageBox.Show("Logado com sucesso!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FormMenu tela = new FormMenu();
                tela.VerificarPermissoes(dto);
                tela.Show();

                MessageBox.Show("Seja bem vindo a Esmalteria Glamuor", "Assistente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "Digite seu Nome")
            {
                txtNome.Text = string.Empty;
            }
        }

        private void txtSenha_Click(object sender, EventArgs e)
        {
            if (txtSenha.Text == " ")
            {
                txtSenha.Text = string.Empty;
            }
        }

        private void txtSenha_MouseClick(object sender, MouseEventArgs e)
        {
            txtSenha.Text = string.Empty;
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }
    }
}


